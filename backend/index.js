const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'reviewer_admin',     
    password: 'reviewer_admin',
    database: 'ReviewCafeSystem'   
})

connection.connect()

const express = require('express')
const app = express()
const port = 4000   

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

//list_reviewer
app.get("/list_reviewer", authenticateToken, (req, res) => {
    let reviewer_id = req.user.user_id

    let query = ` 
    SELECT Reviewer.ReviewerID, Reviewer.ReviewerName, Reviewer.ReviewerSurname, 
           Cafe.CafeName, Cafe.CafeLocation, Post.PostContent, Post.PostTime

    FROM Reviewer, Post, Cafe

    WHERE(Post.ReviewerID = Reviewer.ReviewerID) AND
         (Post.CafeID = Cafe.CafeID) AND
         (Reviewer.ReviewerID = ${reviewer_id}) `

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error querying from reviewer db"
            })
        } else {
            res.json(rows)
        }
    })
})

//post
app.post("/post", authenticateToken, (req, res) => {
    let reviewer_id = req.user.user_id
    let cafe_id = req.query.cafe_id
    let content = req.query.content

    let query = `INSERT INTO Post 
                     (ReviewerID, CafeID, PostContent, PostTime)
                    VALUES( '${reviewer_id}',
                            '${cafe_id}',
                            '${content}',
                            NOW())`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding event succesful"
            })
        }
    });
})

//add cafe
app.post("/add_cafe", (req, res) => {

    let cafe_name = req.query.cafe_name
    let cafe_location = req.query.cafe_location

    let query = `INSERT INTO Cafe
                    (CafeName, CafeLocation)
                    VALUES('${cafe_name}', '${cafe_location}')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding event succesful"
            })
        }
    });
})

//update post 
app.post("/update_post", (req, res) => {

    let post_id = req.query.post_id
    let cafe_id = req.query.cafe_id
    let content = req.query.content

    let query = `UPDATE Post SET
                    CafeID = ${cafe_id},
                    PostContent = '${content}',
                    PostTime = NOW()
                    WHERE PostID = ${post_id}`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error updating record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Updating event succesful"
            })
        }
    });
})

//delete post
app.post("/delete_post", (req, res) => {

    let post_id = req.query.post_id
    let query = `DELETE FROM Post WHERE PostID=${post_id}`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error deleting record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Deleting record success"
            })
        }
    });
})

//register
app.post("/register", (req, res) => {
    let reviewer_name = req.query.reviewer_name
    let reviewer_surname = req.query.reviewer_surname
    let reviewer_username = req.query.reviewer_username
    let reviewer_password = req.query.reviewer_password

    bcrypt.hash(reviewer_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Reviewer
                    (ReviewerName, ReviewerSurname, Username, Password)
                    VALUES ('${reviewer_name}', 
                            '${reviewer_surname}',
                            '${reviewer_username}',
                            '${hash}')`
        console.log(query)

        connection.query(query, (err, rows) => {
            if (err) {
                res.json({
                    "status": "400",
                    "message": "Error inserting data into db"
                })
            } else {
                res.json({
                    "status": "200",
                    "message": "Adding new user succesful"
                })
            }
        });
    })
})

//login
app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.user_password
    let query = `SELECT * FROM Reviewer WHERE Username = '${username}'`
    console.log(query)
    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from Reviewer db"
            })
        } else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username": rows[0].Username,
                        "user_id": rows[0].ReviewerID
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: '1d' })
                    res.send(token)
                } else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})

app.listen(port, () => {
    console.log('Now starting ReviewCafeSystem Backend ' + port)
})